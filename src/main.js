import Vue from 'vue'
import App from './App.vue'
import store from './store'
import 'element-ui/lib/theme-chalk/index.css'
import { Dialog, Button, Icon } from 'element-ui'
Vue.config.productionTip = false
Vue.use(Dialog)
Vue.use(Button)
Vue.use(Icon)
new Vue({
  store,
  render: h => h(App)
}).$mount('#app')
