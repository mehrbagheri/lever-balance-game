export const MAX_BENDING_ANGLE = 30
export const MIN_BENDING_ANGLE = -30
export const LEVER_HEIGHT = 6
export const LEVER_WIDTH = 100
