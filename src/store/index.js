import Vue from 'vue'
import Vuex from 'vuex'
import {
  MAX_FALLING_INTERVAL_GAP,
  MIN_FALLING_INTERVAL_GAP,
  MIN_WEIGHT,
  MAX_WEIGHT,
  SCALE_STEP
} from '@/constants/shape'
import {
  MAX_BENDING_ANGLE,
  MIN_BENDING_ANGLE
} from '@/constants/teeter-totter'
import { uuid } from 'uuidv4'
import { getShapesProportion } from '@/utils/shapes'
import { generateRandomNumber } from '@/utils/numbers'
import { generateRandomDarkHSLColor } from '@/utils/colors'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    load: [],
    effort: [],
    gamePaused: true,
    incomingShapes: [],
    dialogVisible: false
  },
  mutations: {
    addEffort (state, shape = {}) {
      state.effort.push(shape)
    },
    resetAll (state) {
      state.incomingShapes = []
      state.effort = []
      state.load = []
    },
    toggleGame (state) {
      state.gamePaused = !state.gamePaused
    },
    generateShape ({ load, incomingShapes }, isLoad = false) {
      const top = 0
      const id = uuid()
      const color = generateRandomDarkHSLColor()
      const type = generateRandomNumber(1, 2)
      const left = generateRandomNumber(0, 40)
      const weight = generateRandomNumber(MIN_WEIGHT, MAX_WEIGHT - 1)
      const scale = 1 + SCALE_STEP * (weight - 1)
      const shape = { id, color, left, scale, top, type, weight }
      if (isLoad) {
        return load.push(shape)
      }
      incomingShapes.push(shape)
    },
    moveShape ({ incomingShapes }, { moveLeft, width }) {
      const shape = incomingShapes[0]
      const canMoveLeft = shape.left - 1 >= 0
      const canMoveRight = shape.left + width + 1 <= 45
      if (moveLeft) {
        if (canMoveLeft) {
          shape.left--
        }
      } else {
        if (canMoveRight) {
          shape.left++
        }
      }
    },
    toggleModal (state, isShown = false) {
      state.dialogVisible = isShown
    },
    updateFallingIntervalGap (state, reset = false) {
      if (reset) {
        state.fallingIntervalGap = MAX_FALLING_INTERVAL_GAP
      } else if (state.fallingIntervalGap > MIN_FALLING_INTERVAL_GAP) {
        state.fallingIntervalGap--
      }
    }
  },
  getters: {
    leverBendingAngle (state, { effortSum, loadSum }) {
      const balanceThreshold = 50
      if (!effortSum) {
        return MAX_BENDING_ANGLE
      }
      const subtraction = Math.abs(effortSum - loadSum)
      return effortSum > loadSum ? subtraction / effortSum * (balanceThreshold * -1) : subtraction / loadSum * balanceThreshold
    },
    effortSum ({ effort }) {
      return getShapesProportion(effort, true)
    },
    loadSum ({ load }) {
      return getShapesProportion(load)
    },
    isLeverAngleWithinLimits (state, { leverBendingAngle }) {
      return leverBendingAngle > MIN_BENDING_ANGLE &&
       leverBendingAngle < MAX_BENDING_ANGLE
    }
  },
  actions: {
    initGame ({ commit, state }) {
      if (!state.gamePaused) {
        commit('toggleGame')
      }
      if (state.dialogVisible) {
        commit('toggleModal')
      }
      commit('updateFallingIntervalGap', true)
      commit('resetAll')
      commit('generateShape')
      commit('generateShape', true)
    }
  }
})
